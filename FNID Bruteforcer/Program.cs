﻿using Extra.Utilities;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace FNID_Bruteforcer
{
    internal class Program
    {
        private const string salt = "6759659904250490566427499489741A";
        private static string dictionary;
        private static byte[] expectedHash;

        private static void GenerateDictionary(string prefix, int position, ref int maxCharacters)
        {
            position++;

            foreach (char name in dictionary)
            {
                byte[] hash = GetHash(prefix + name);

                bool foundMatch = CryptographicEngines.CompareBytes(hash, hash.Length - 4, expectedHash, 0, expectedHash.Length);

                if (foundMatch)
                {
                    string hashStr = BitConverter.ToString(hash).Replace("-", "");

                    hashStr = hashStr.Remove(0, hashStr.Length - 8);

                    Log(hashStr + " == " + prefix + name);
                }

                if (position < maxCharacters)
                    GenerateDictionary(prefix + name, position, ref maxCharacters);
            }
        }

        private static byte[] GetHash(string fnid)
        {
            using (SHA1Cng sha1 = new SHA1Cng())
            {
                byte[] fnidRawBytes = Encoding.ASCII.GetBytes(fnid);

                string fnidRawStr = BitConverter.ToString(fnidRawBytes).Replace("-", "") + salt;

                byte[] fnidRaw = MiscUtils.HexStringToByteArray(fnidRawStr);

                byte[] fnidHash = sha1.ComputeHash(fnidRaw);

                MiscUtils.CheckEndianness(ref fnidHash);

                return fnidHash;
            }
        }

        private static void Initialization(string[] args, out string prefix, out int unknownLength)
        {
            prefix = args[0];

            expectedHash = MiscUtils.HexStringToByteArray(args[1]);

            bool lengthParsed = int.TryParse(args[2], out unknownLength);

            if (!lengthParsed)
                unknownLength = 9;

            if (args.Length != 4)
            {
                dictionary = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";

                return;
            }

            int dictionaryMode;
            bool dictionaryModeParsed = int.TryParse(args[3], out dictionaryMode);

            if (!dictionaryModeParsed)
                dictionaryMode = 0;

            switch (dictionaryMode)
            {
                case 0:
                default:
                    dictionary = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
                    break;

                case 1:
                    dictionary = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    break;

                case 2:
                    dictionary = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_";
                    break;

                case 3:
                    dictionary = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    break;
            }
        }

        private static void Log(string message)
        {
            Console.Write(message + Environment.NewLine);

            File.AppendAllText("results.log", message + Environment.NewLine);
        }

        private static void Main(string[] args)
        {
            if (args.Length < 3 || args.Length > 4)
            {
                Console.WriteLine("FNID Bruteforcer by Dasanko, written for MrX." + Assembly.GetExecutingAssembly().GetName().Version);

                Console.WriteLine("Usage:" + Environment.NewLine + "FNID_Bruteforcer <prefix> <hash> <max_length> [optional: dictionary_mode]");

                Console.WriteLine("Dictionary mode is optional. 0 is the default mode and it will use all characters, 1 for letters only, 2 for letters and the underscore symbol, 3 for letters and numbers.");

                Console.WriteLine("Example:" + Environment.NewLine + "FNID_Bruteforcer cellCryptoPuEccEcDsa B80602D2 4");

                Console.WriteLine("Example:" + Environment.NewLine + "FNID_Bruteforcer cellCryptoPuEccEcDsa B80602D2 4 1");

                return;
            }

            string prefix;
            int unknownLength;
            Initialization(args, out prefix, out unknownLength);

            for (int i = 1; i <= unknownLength; i++)
            {
                var sw = Stopwatch.StartNew();

                GenerateDictionary(prefix, 0, ref i);

                Console.WriteLine("Looping through position " + i + " has taken: " + sw.Elapsed);
            }

            Console.WriteLine("Finished.");

            Console.ReadKey(true);
        }
    }
}
